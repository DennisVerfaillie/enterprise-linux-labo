## Laboverslag: DHCP

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures


- Voeg in het bestand vagrant_hosts.yml volgende lijnen toe

```
- name: pr001
  ip: 172.16.0.2
```

- Voeg in het bestand site.yml volgende lijnen toe

```
- hosts: pr001  
  sudo: true  
  roles:  
    - bertvv.el7  
    - bertvv.dhcp
```

- Voer in Git Bash het scriptje role-deps.sh uit  
- Maak in het mapje host_vars een nieuw bestand pr001.yml aan  
- Voeg de volgende lijnen toe in dit bestand  

```
dhcp_global_subnet_mask: 255.255.0.0
dhcp_global_broadcast_address: 172.16.255.255
dhcp_global_routers: 172.16.255.254
dhcp_global_domain_name: linuxlab.lan
dhcp_global_domain_name_servers:
  - 192.0.2.10
  - 192.0.2.11
dhcp_subnets:
  - ip: 172.16.0.0
    netmask: 255.255.0.0
    domain_name_servers:
      - 192.0.2.10
      - 192.0.2.11
    range_begin: 172.16.0.1
    range_end: 172.16.255.254
    hosts:
      - name: pr011
        mac: '08:00:27:81:b3:92'
        ip: 172.16.0.11
      - name: TestLinux
        mac: '08:00:27:F2:EF:B9'
        ip: 172.16.10.15
```


### Testplan en -rapport

* Voer het commando `systemctl status dhcpd.service` uit op machine pr001  
    * De DHCP service is actief    
  
* Maak (manueel) een nieuwe VirtualBox VM met twee host-only netwerkinterfaces die beide aangesloten zijn op het netwerk met IP 172.16.0.0/16  

* Noteer van de ene interface het MAC-adres en definieer een reservatie voor dit adres

* Controleer of beide netwerkinterfaces een correct ip-adres toegewezen krijgen  
    * De eerste netwerkinterface krijgt ip-adres 172.16.10.15 toegewezen, zoals gedefinieerd in het configuratiebestand
    * De tweede netwerkinterface krijgt een ip-adres toegekregen in de range 172.16.100.1 t/m 172.16.255.253 

  
### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Het maken van deze opdracht ging heel vlot, helemaal niet veel problemen mee gehad. De eerste keer dat ik het probeerde met de virtuele machine erbij krag hij direct een ip-adres.

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- Geen problemen gehad bij deze opdracht

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

- Na een heel semester te werken met rollen viel deze opdracht zeker goed mee. Enkel het werken met de DHCP-rol heb ik dus bijgeleerd.

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

- Ik heb geen vragen meer over deze opdracht.

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.

- www.github.com/bertvv
