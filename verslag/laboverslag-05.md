## Laboverslag: Fileserver

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures

#### Samba

- Voeg in het bestand vagrant_hosts.yml volgende lijnen toe

```
- name: pr011
  ip: 172.16.0.11
```

- Voeg in het bestand site.yml volgende lijnen toe

```
	- hosts: pr011  
  	  sudo: true  
  	  roles:  
    	- bertvv.el7  
    	- bertvv.samba  
```

- Voer in Git Bash het scriptje role-deps.sh uit
- Maak in het mapje host_vars een nieuw bestand pr011.yml aan
- Voeg de volgende lijnen toe in dit bestand

```  
	samba_netbios_name : FILES
	samba_workgroup: LINUXLAB
	samba_load_homes: true
	samba_users:
  	- name: franka
      password: franka
  	- name: femkevdv
      password: femkevdv
  	- name: hansb
      password: hansb
  	- name: kimberlyvh
      password: kimberlyvh
	- name: taniav
	  password: taniav
	- name: peterj
	  password: peterj
	- name: maaiked
	  password: maaiked
	- name: dennis
	  password: dennis
    
	samba_shares_root: /srv/shares
    
	samba_shares:
  	  - name: directie
    	comment: 'Alle shares voor directie'
    	group: directie
    	write_list: dennis,maaiked,franka,femkevdv
    	valid_users: dennis,maaiked,franka,femkevdv
    
  	  - name: financieringen
    	comment: 'alle shares voor dienst financieringen'
    	group: financieringen
    	write_list: franka,dennis,maaiked,peterj
    	valid_users: franka,dennis,maaiked,peterj,femkevdv,hansb,kimberlyvh,taniav
    
  	  - name: staf
    	group: staf
    	write_list: dennis,maaiked,franka,femkevdv
    	valid_users: franka,dennis,maaiked,peterj,femkevdv,hansb,kimberlyvh,taniav
    
  	  - name: verzekeringen
    	group: verzekeringen
    	write_list: dennis,maaiked,franka,hansb,kimberlyvh,taniav
    	valid_users: dennis,maaiked,franka,hansb,kimberlyvh,taniav,femkevdv,peterj
    
  	  - name: publiek
    	group: publiek
    	write_list: franka,femkevdv,hansb,kimberlyvh,taniav,peterj,maaiked,dennis
    	valid_users: franka,femkevdv,hansb,kimberlyvh,taniav,peterj,maaiked,dennis
  
  	  - name: beheer
    	group: beheer
    	write_list: dennis,maaiked
    	valid_users: dennis,maaiked
  
  	  - name: franka
		write_list: franka
		valid_users: franka
    
  	  - name: femkevdv
		write_list: femkevdv
		valid_users: femkevdv
  
  	  - name: hansb
		write_list: hansb
		valid_users: hansb
    
  	  - name: peterj
		write_list: peterj
		valid_users: peterj
    
  	  - name: kimberlyvh
		write_list: kimberlyvh
		valid_users: kimberlyvh
    
  	  - name: taniav
		write_list: taniav
		valid_users: taniav
    
  	  - name: maaiked
		write_list: maaiked
		valid_users: maaiked
    
  	  - name: dennis
		write_list: dennis
		valid_users: dennis
```

- Voeg onderstaande code toe in all.yml

```
 el7_users:
  - name: dennis
    password: dennis
    comment: Administrator
    groups: 
      - wheel
      - publiek
      - directie
      - financieringen
      - staf
      - verzekeringen
      - beheer
  - name: franka
    password: franka
    comment: Directie
    shell: /sbin/nologin
    groups: 
      - directie
      - staf
      - financieringen
      - verzekeringen
      - publiek
  - name: femkevdv
    password: femkevdv
    comment: Staf
    shell: /sbin/nologin
    groups:
      - staf
      - publiek
  - name: hansb
    password: hansb
    comment: Verzekeringen
    shell: /sbin/nologin
    groups:
      - verzekeringen
      - publiek
  - name: kimberlyvh
    password: kimberlyvh
    comment: Verzekeringen
    shell: /sbin/nologin
    groups: 
      - verzekeringen
      - publiek
  - name: taniav
    password: taniav
    comment: Verzekeringen
    shell: /sbin/nologin
    groups:
      - verzekeringen
      - publiek
  - name: peterj
    password: peterj
    comment: Financieringen
    shell: /sbin/nologin
    groups:
      - financieringen
      - publiek
  - name: maaiked
    password: maaiked
    comment: Administrator
    groups:
      - directie
      - financieringen
      - staf
      - verzekeringen
      - publiek
      - beheer
  - name: dennisv
    password: test
    comment: Administrator
    groups:
      - directie
      - financieringen
      - staf
      - verzekeringen
      - publiek
      - beheer

el7_firewall_allow_services:
  - http
  - https
  - dns
  - samba
```

- Maak nieuwe testen aan in het bestand samba.bats

```
@test "check group write access on files" {
  #                         Share           User1         Passwd1       User2         Passwd2
  assert_group_write_file   directie        dennis        dennis        maaiked       maaiked
  assert_group_write_file   financieringen  dennis        dennis        franka        franka
  assert_group_write_file   staf            dennis        dennis        femkevdv      femkevdv
  assert_group_write_file   verzekeringen   dennis        dennis        taniav        taniav
  assert_group_write_file   publiek         dennis        dennis        hansb         hansb
  assert_group_write_file   beheer          dennis        dennis        maaiked       maaiked
  assert_group_write_file   verzekeringen   kimberlyvh    kimberlyvh    hansb         hansb
}

@test "check group write access on directories" {
  #                         Share           User1         Passwd1       User2         Passwd2
  assert_group_write_dir    directie        franka        franka        maaiked       maaiked
  assert_group_write_dir    financieringen  peterj        peterj        dennis        dennis
  assert_group_write_dir    staf            femkevdv      femkevdv      franka        franka
  assert_group_write_dir    verzekeringen   hansb         hansb         kimberlyvh    kimberlyvh
  assert_group_write_dir    publiek         dennis        dennis        hansb         hansb
  assert_group_write_dir    beheer          maaiked       maaiked       dennis        dennis
}
```

#### vsftpd

Eerst dacht ik dat het vlotter ging gaan om een bestaande rol aan te passen, maar dit is zeker niet gemakkelijker en daarom leek het mij een beter idee om van nul te beginnen en mijn eigen rol te schrijven aan de hand van uw Ansible-role-skeleton.

- Om te beginnen moet je de volgende handler aanmaken die later gebruikt wordt

```
- name: restart vsftpd
  service:
    name: vsftpd
    state: restarted
```

- Vergeet ook zeker in de meta je naam niet te vermelden, zodat mensen weten dat jij de auteur van deze rol bent.

- Maar het belangrijkste deel van de rol bevindt zich in de tasks, voeg het volgende hier toe

```
- name: Install packages
  yum:
    pkg: "{{ item }}"
    state: installed
  with_items: vsftpd
  tags: vsftpd
  
- name: Start vsftpd service
  service:
    name: vsftpd
    state: started
    enabled: yes
  
- name: Install config file
  template:
    src: etc-vsftpd.conf2.j2
    dest: /etc/vsftpd/vsftpd.conf
  notify: restart vsftpd
  tags: vsftpd
```

- bij de tasks maak je gebruik van een template, dit template vind je terug in de map templates. Maak daar deze template aan

```
anonymous_enable=NO
chroot_list_enable=NO
chroot_list_file=/etc/vsftpd/chroot_list
local_enable=YES
write_enable=YES
chroot_local_user=YES
listen_ipv6=NO
guest_enable=NO
connect_from_port_20=YES
listen=YES
```

### Testplan en -rapport

* Voer het commando `sudo /vagrant/test/runbats.sh` uit    
Alle tests worden gerund en ze slagen    
  
* Ga op de lokale machine in de verkenner naar \\files  
Ik kan alle shares zien die aangemaakt werden op de fileserver  
  
* Ga op de lokale machine in de verkenner naar ftp://172.16.0.11/  
Ik krijg een aanmeldvenster voor mij en wanneer ik een gebruikersnaam en wachtwoord ingetikt heb kan ik me aanmelden.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Werken met rollen is ondertussen al vanzelfsprekend geworden
- Shares aanmaken en er de personen aan toekennen ging vlug
- Zelf de testen schrijven lukte direct dankzij hulpfunctie die al was aangemaakt
- De eigen rol aanmaken via het verkregen skelet was redelijk vanzelfsprekend


#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- Op dit moment heb ik de namen van de users gebruikt om toegang te krijgen tot de shares, werken met groepen lukte in eerste instantie nog niet
- Zelf een rol schrijven is een grote uitdaging
- Aangezien ik 2 gebruikers met de naam dennis had heeft mij dit lang fouten opgeleverd
- Eenmaal een gebruiker aangemaakt is, veranderen zijn instellingen duidelijk niet door enkel vagrant provision uit te voeren
- De gebruikers aanmaken voor vsftpd begreep ik nog niet zo heel goed

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

- Opfrissing van hoe gedeelde mappen en toegangsrechten enzovoort werken
- Hoe ik op de hostmachine gedeelde mappen moet zoeken
- Zelf een rol aanmaken om te gebruiken om dingen te automatiseren

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

- Vsftpd gebruikers aanmaken..

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
