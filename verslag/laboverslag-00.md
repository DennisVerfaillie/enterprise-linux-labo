## Laboverslag: Opzetten werkomgeving

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/Enterprise-linux-labo

### Procedures

1. Installeren nodige software
2. Registratie BitBucket
3. Configureer Git
	- $ git config --global user name DennisVerfaillie
	- $ git config --global user email dennis.verfaillie.x7529@student.hogent.be
	- $ git config --global push.default simple
	
4. Configuratie ssh sleutelpaar
	- ssh-keygen -t rsa
	- kies standaard locatie en stel een pass phrase in
	- open id_rsa.pub bestand en kopieer je ssh-key
	- ga naar je bitbucket en plak de sleutel op de pagina SSH
	
5. maak lokale kopie van de repository
	$ git clone --config core.autocrlf=input git@bitbucket.org DennisVerfaillie/Enterprise-linux-labo.git

6. maak VM aan m.b.v. vagrant
	- $ vagrant up

7. voeg rollen toe aan pu004
	- vi site.yml
	- voeg bertvv.el7 toe bij roles
	
8. run script om rollen te downloaden en installeren
	- bash scripts/role-deps.sh
	- vagrant provision
	
9. Toepassen instellingen op alle hosts
	
	- vi ansible/var_groups/all.yml
	
el7_admin_user: dennis

el7_install_packages: 

  - bash-completion
  - bind-utils
  - git
  - nano
  - tree
  - vim-enhanced
  - wget
  
el7_admin_ssh_key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCrtQPxAet8qwMLBy3F1b/ruNeuaRr6O6MpWeu9MSX0ZhbxgcWzxWmiECQrn28kEeV+AHBang4epYXJrhMFCAcw9r/HEY62vgIcEqSoEeZXIt6VHil2j3dTtyaPd09kgUM/SEbJRic4oC5E1inW8SY5t/2GMxkzonEwJJ3RAhwgf+ahoL7EeE4b+2Fl/2xzk9m3ejcjA9QhVTNy6CuTY3yBP0E8kNMPE7AjOqSTF+zbmuukqKr88K7OmFFxMdtHltMJVSOh5m5jTJHNHo7Hd3jQ+GWRueqInJoNj30dqZDpoAR+fCKJ6PO78GNTJGaTkQheSmO2I7a6fI5T6Lm0+6HPnSO7C5dkW9rZXoG+oYdkD3DcM+Oh76+rxJCC65mcnt5Pp1PfnZ1unTK1P3Ka8YRxjh4SanLDD6F5x5ngL4XUFAjD8efH2xP3NOmg4Y+F+ZQnEX9m+elpVaKtKGruq1wC97XTTrP13TrpAt9xZRN7srng87SDpOPpi8JTg/wk62LDj3YZ2seNZr1abAlpCnrp+jRnG9irCeE8le8dlzJVFMlVv17msCATlhXhYxiE5/e6ZCXk9ZAwbVRIRJwJEFj3gdrpBlEdhDh9i0CEVjN5s6hEoBXdspzYDYfJlgprvcJpszDuZG5W3hFmvnR3Tj1m5VSahDFLeu7Wsza/hEpd0w=='

el7_users:
  - name: dennis
    password: '$6$MdwghPUVu.e2rzW1$QyCKmlUtPFKL62zhkhuksJrlailOECVdzB1E6JzI5xQJI0.5eR5wlVvYmKR4l4KBe0RXUfWENG1Y3N/pHHhpb/'
    comment: 'Administrator'
    groups: 
      - wheel
	  
el7_motd: true

el7_repositories: epel-release


wijzigingen doorvoeren
 - vagrant provision
 
 10. run de testen
 - sudo /vagrant/test/runbats.sh
	
	
### Testplan en -rapport

0. Ga op het hostsysteem naar de directory met de lokale kopie van de repository.

1. Voer vagrant status uit

• je zou één VM moeten zien met naam pu004 en status not created. Als deze toch bestaat, doe dan eerst
vagrant destroy -f pu004.
2. Voer vagrant up pu004 uit.
• Het commando moet slagen zonder fouten (exitstatus 0)

-> Het commando slaagt en de virtuele machine wordt aangemaakt.

3. Log in op de server met vagrant ssh srv010 en voer de acceptatietests uit:

[vagrant@pu004 test]$ sudo /vagrant/test/runbats.sh
Running test /vagrant/test/common.bats
✓ EPEL repository should be available
✓ Bash-completion should have been installed
✓ bind-utils should have been installed
✓ Git should have been installed
✓ Nano should have been installed
✓ Tree should have been installed
✓ Vim-enhanced should have been installed
✓ Wget should have been installed
✓ Admin user bert should exist
✓ Custom /etc/motd should be installed
10 tests, 0 failures

-> de testen worden uitgevoerd en alles slaagt.

4. Log uit en log vanop het hostsysteem opnieuw in, maar nu met ssh. Er mag geen wachtwoord gevraagd worden.

$ ssh bert@192.0.2.50
Welcome to pu004.localdomain.
enp0s3 : 10.0.2.15 fe80::a00:27ff:fe5c:6428/64
enp0s8 : 192.0.2.50 fe80::a00:27ff:fecd:aeed/64
[bert@pu004 ~]$

-> ik kan me aanmelden zonder dat er een wachtwoord wordt gevraagd.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Ik kon redelijk vlug werken met de Bash-omgeving, omdat we dit vorig jaar in het project ook moesten gebruiken.
- De VM aanmaken ging zonder problemen
- de SSH-key toevoegen aan bitbucket had ik vlug gevonden

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- De tests runnen heeft redelijk lang geduurd
- Het bestand all.yml bewerken totdat het aan de voorwaarden voldeed heeft me lang gekost.

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

- De syntax van YAML bestanden heb ik nu geleerd.
- Verder leren werken met de bash-omgeving
- ssh-sleutels leren toevoegen

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

- op dit moment werkt alles.

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
- https://github.com/bertvv/ansible-role-el7
- https://help.github.com/articles/generating-ssh-keys/