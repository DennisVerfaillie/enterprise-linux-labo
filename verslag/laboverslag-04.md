## Laboverslag: Troubleshooting

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures

Ip adres testen: ip a

	- e.g. vbox NAT: 10.0.2.15
	
	- e.g. vbox host-only: 192.168.56.101
	
	- /etc/sysconfig/network-scripts/ifcfg-IFACE
	
NO-CARRIER  misschien de kabel insteken?

Nog niet opgelost -->

Machine zelf 

-	Ip-adres en netwerkmasker

-	Default-gateway

	Ip r

	Vbox NAT: 10.0.2.2

	Thuisnetwerk: 192.168.0.1

-	Dns-server

	Cat /etc/resolv.conf

	Nat: 10.0.2.3

	Thuis: zelfde als gateway

Nog niet --> 

	Default gateway: ping

	Dns request: evt ping, dig, host, nslookup

	Ping andere host binnen subnet

	Traceroute naar buiten

	Traceroute, tracert

Zijn de juiste poorten open/draait de service

-	Sudo systemctl status httpd.service
-	Sudo ss –tulpn: 80, 443 zeker open
-	Sudo ps –ef

Wordt de poort gefilterd door de firewall?

-	Sudo firewall-cmd –list-all
-	Sudo iptables –L –n –v

Bekijk de logfiles

-	Sudo journalctl –l –f –u httpd.service

Valideer configuratiebestanden

-	Dns: named-checkconf, named-checkzone

### Testplan en -rapport

- Kun je de service starten?  
    Ja, die start.

- Kun je je aanmelden via de host?  
    Ja, dit lukt.
    
- Kun je de tests runnen en slagen deze?  
    Ja, de test werken.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- uiteindelijk heb ik alle problemen kunnen vinden en kunnen oplossen.

#### Wat ging niet goed?

- moeilijk om alle fouten te vinden
- redelijk wat samengewerkt met andere mensen om alles op te lossen

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.
- alles goed overlezen, zelfs het kleinste haakje op de verkeerde plaats kan ervoor zorgen dat alles faalt.
- werken met de configuratiebestanden van BIND

#### Waar heb je nog problemen mee?

- voor deze opdracht is alles gelukt.

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
