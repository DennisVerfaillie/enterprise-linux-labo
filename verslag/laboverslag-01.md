## Laboverslag: Actualiteit

- Naam cursist: DennisVerfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures

##### Cheat-sheet

Voor het aanvullen van de cheat-sheet heb ik mij gebaseerd op alle commando's die ik tegenkwam tijdens het maken van de verschillende labo's voor het OLOD Enterprise Linux. Zo is er onderandere een lijst van handige commando's om te navigeren binnen een linux-besturingssysteem. Maar dan ook een lijst die toont hoe je moet werken met rollen en hoe je zelf een rol moet aanmaken. En ten laatste is er ook nog een onderdeel aanwezig dat gaat over troubleshooting, waarin ik de commando's en methodes heb gezet die ik gebruikt heb tijdens het maken van de troubleshooting opdrachten.

##### Nieuwe technieken uitproberen

Mijn eerste nieuwe techniek die ik uitgewerkt heb is die om .yml files te encrypteren. Dit is zeker een belangrijk onderdeel aangezien veiligheid in het echte leven van groot belang is. Het is daarom van het grootste belang dat de wachtwoorden in die bestanden niet zomaar door iedereen te bekijken zijn.


1. Als eerste stap gaan we onze bestanden die we reeds gemaakt hebben gaan encrypteren  
  `ansible-vault encrypt /ansible/group_vars/all.yml`

2. Nadien vraagt de prompt om een paswoord, geef dit in

3. Om nadien een playbook te runnen dat gebruik maakt van een Role met een geëncrypteerd bestand gebruik je volgend commando
  `ansible playbook --ask-vault-pass provision.yml`
  
  


Ik heb ook nog een tweede techniek gekozen aangezien ik beide technieken persoonlijk heel erg interessant vind. Mijn tweede techniek is van Johnson (2015) die een methode heeft uitgewerkt om Ansible te versnellen. Ik koos hiervoor omdat dit ook zelf handig is wanneer iets vlugger werkt. Op die manier is er minder tijdsverlies en kan er efficënter gewerkt worden.

1. De eerste stap hier is logischerwijs data verzamelen waarmee je nadien kan zien of er zich effectief een verbetering in de performantie voordoet. Wat je hiervoor moet doen is een kleine plugin installeren die de data zal verzamelen.  

  - mkdir callback_plugins  
  - cd callback_plugins  
  - wget https://raw.githubusercontent.com/jlafon/ansible-profile/master/callback_plugins/profile_tasks.py  

  Wanneer je nadien bijvoorbeeld een virtuele machine vernietigt en daarna weer aanmaakt krijg je te zien hoelang iedere stap geduurd heeft.

2. SSH pipelining: deze instelling zorgt ervoor dat ansible modules uitgevoerd worden zonder dat er bestanden verplaatst worden.

  - Zorg ervoor dat in het bestand /etc/sudoers 'requiretty' ingesteld is op disabled
  - Voeg in het bestand ansible.cfg het volgende toe:
	`[ssh_connection]`
	`pipelining = True`
		
3. ControlPersist: dit is een SSH eigenschap die de connectie met de server openhoudt, klaar om direct te hergebruiken. Wanneer dit aanstaat hoeft ansible niet opnieuw te verbinden met hosts voor iedere taak.

  - Voeg opnieuw in ansible.cfg het volgende toe:
	 `control_path = /tmp/ansible-ssh-%%h-%%p-%%r`
	
Dit zou opnieuw 10-20% van de tijd moeten verminderen.


##### Bijdrage aan een open source project

Als mijn bijdrage aan een open source project heb ik ervoor gekozen om iets te doen voor OpsSchool. Dit heb ik gedaan omdat ik zelf ook erg vaak gebruik maak van dit soort online handboeken, en aangezien er daar veel mensen heel veel tijd insteken vind ik het nodig om zelf ook eens mijn bijdrage te leveren hieraan. Hopelijk zal ik zo ook in de toekomst iemand helpen die vlug iets moet opzoeken. Zo moest er nog een onderdeel toegevoegd worden over wat PKI is. Dit heb ik dan zelf ook opgezocht en aangevuld. Ik moest antwoorden op 3 vragen, wat is PKI, waardoor wordt het gebruikt en waarom is het belangrijk.


Public Key Cryptography
=======================
   
A public key infrastructure (PKI) supports the distribution and identification
of public encryption keys, enabling users and computers to both securely exchange
data over networks suck as the internet and verify the identity of the other party.
Without PKI, sensitive information can still be encrypted and exchanged, but there
would be no assurance of the identity of the other party. Any form of sensitive
data exchanged over the internet is reliant on PKI for security.

A typical PKI consists of hardware, software, policies and standards to manage the
creation, administration, distribution and revocation of keys and digital 
certificates. Digital certificates are most important parts of PKI as they affirm
the identity of the certificate subject and bind that identity to the public 
key contained in the certificate.

PKI includes the following key elements: 

- A trusted party, called a certificate authority (CA), acts as the root of trust
  and provides services that authenticate the identity of individuals, computers 
  and other entities  
- A registration authority, often called a subordinate CA, certified by a root CA
  to issue certificates for specific uses permitted by the root  
- A certificate database, which stores certificate requests and issues and revokes
  certificates 
- A certificate store, which resides on a local computer as a place to store 
  issued certificates and private keys  

Using public and private keys for SSH authentication
----------------------------------------------------

PKI can be used when you want to send a secure email to another person. I will 
use an example in which Bob wants to send an email to Alice. This can be 
accomplished in the following manner:

1. Both Bob and Alice have their own key pairs. They have kept their private 
  keys securely to themselves and have sent their public keys directly to 
  each other.
2. Bob uses Alice's public key to encrypt the message and sends it to her.
3. Alice uses her private key to decrypt the message.

This simplified example highlights at least one obvious concern Bob must have 
about the public key he used to encrypt the message. That is, he cannot know 
with certainty that the key he used for encryption actually belonged to Alice.
It is possbile that another party monitoring the communication channel between
Bob and Alice substituted a different key.

The public key infrastructure concept has evolved to help address this problem 
and others. When a CA is used, the preceding example can be modified in the 
following manner:


1. Assume that the CA has issued a signed digital certificate that contains its 
   public key. The CA self-signs this certificate by using the private key 
   that corresponds to the public key in the certificate.
2. Alice and Bob agree to use the CA to verify their identities.
3. Alice requests a public key certificate from the CA.
4. The CA verifies her identity, computes a hash of the content that will make 
   up her certificate, signs the hash by using the private key that corresponds 
   to the public key in the published CA certificate, creates a new certificate
   by concatenating the certificate content and the signed hash, and makes the 
   new certificate publicly available.
5. Bob retrieves the certificate, decrypts the signed hash by using the public 
   key of the CA, computes a new hash of the certificate content, and compares
   the two hashes. If the hashes match, the signature is verified and Bob can 
   assume that the public key in the certificate does indeed belong to Alice.
6. Bob uses Alice's verified public key to encrypt a message to her.
7. Alice uses her private key to decrypt the message from Bob.  
	
Dit zou opnieuw 10-20% van de tijd moeten verminderen.


### Testplan en -rapport


### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Doorheen het semester heb ik mijn cheat-sheet bijgehouden
- Ik had interesse in mijn bijdrage aan een open-source project, ik hoop dat ik nuttig werk heb verricht dat iemand anders later kan helpen
- De nieuwe technieken die ik heb uitgeprobeerd zijn interessant en erg handig

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- Redelijk lang deze taak laten liggen, pas op het einde echt aan beginnen werken.

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

- Zelf mogelijke interessante onderwerpen zoeken die te maken hebben met een linux besturingssysteem
- Hoe je zelf een bijdrage kunt leveren aan een open-source project, dit is veel eenvoudiger dan ik eerst had gedacht

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?


### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.  

http://searchsecurity.techtarget.com/definition/PKI  
https://msdn.microsoft.com/en-us/library/windows/desktop/bb427432%28v=vs.85%29.aspx  
https://nl.wikipedia.org/wiki/Public_key_infrastructure  
https://docs.ansible.com/ansible/playbooks_vault.html  
https://gist.github.com/tristanfisher/e5a306144a637dc739e7  
http://adamj.eu/tech/2015/05/18/making-ansible-a-bit-faster/  