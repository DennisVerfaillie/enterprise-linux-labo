## Laboverslag: DNS

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures

1. Voeg in het bestand site.yml de volgende lijnen toe. Deze zorgen ervoor dat er 2 extra virtuele machines aangemaakt kunnen worden met de correcte geïnstalleerde packages.  
    
```
- hosts: pu001  
  sudo: true  
  roles:  
    - bertvv.el7  
    - bertvv.bind  
      
- hosts: pu002  
  sudo: true  
  roles:  
    - bertvv.el7  
    - bertvv.bind  
```
    
2. Maak in de map ansible een nieuwe map genaamd host_vars
3. Maak in deze map de bestanden pu001.yml, pu002.yml, pu004.yml
4. Zet onderstaande code in pu001.yml
    
``` 
bind_zone_name: 'linuxlab.lan'

bind_zone_networks:
  - '192.0.2'
  - '172.16'
  
bind_listen_ipv4:
  - 'any'

bind_zone_master_server_ip: '192.0.2.10'
bind_zone_name_servers:
  - 'pu001'
  - 'pu002'
  
bind_zone_mail_servers:
  - name: 'pu003'
    preference: '10'

bind_zone_hosts:
  - name: 'pu001'
    ip: '192.0.2.10'
    aliases: 
      - ns1
  - name: 'pu002'
    ip: '192.0.2.11'
    aliases:
      - ns2
  - name: 'pu003'
    ip: '192.0.2.20'
    aliases:
      - mail
  - name: 'pu004'
    ip: '192.0.2.50'
    aliases:
      - www
  - name: 'pr001'
    ip: '172.16.0.1'
    aliases:
      - dhcp
  - name: 'pr002'
    ip: '172.16.0.2'
    aliases:
      - directory
  - name: 'pr010'
    ip: '172.16.0.10'
    aliases:
      - inside
  - name: 'pr011'
    ip: '172.16.0.11'
    aliases:
      - files
```
    
5. Zet diezelfde code in pu002  
6. Zet onderstaande code in pu004 (knip deze uit all.yml want daar wordt deze code overbodig)  

```
httpd_scripting: 'php'

mariadb_databases:
  - wordpress

mariadb_users:
  - name: admin
    password: admin
    priv: wordpress.*:ALL
    
mariadb_root_password: admin

wordpress_database: wordpress
wordpress_user: admin
wordpress_password: admin
      
```

7. Download de testen van Chamilo, maak daarna in het mapje test 2 nieuwe mappen pu001 en pu002  
8. Plaats het bestand masterdns.bats in pu001 en slavedns.bats in pu002


### Testplan en -rapport

- voer het commando sudo /vagrant/test/runbats.sh uit op iedere virtuele machine. Alle tests zouden moeten slagen  
    --> Al deze tests slagen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Opnieuw met behulp van de BIND rol en de uitleg op de de github pagina van bertvv ging het heel vlot om deze taken uit te voeren.

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- De slaveDNS wilde in eerste instantie nog niet werken, blijkt dat ik gewoon nog was vergeten vagrant provision uit te voeren.

#### Wat heb je geleerd?

- Hoe een DNS server aan te maken met Ansible
- Hoe een slave- en master dns server precies in elkaar zitten
- Werken met de zonebestanden
- Waar al deze bestanden precies te vinden zijn op linux-systemen

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

- Ik heb alles kunnen oplossen, alles werkt.

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.

- github.com/bertvv