## Laboverslag: Troubleshooting 2

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures

1. Zorg ervoor dat nmb aan het runnen is: `sudo systemctl enable nmb`  
2. Samba toevoegen aan firewall-instellingen  
`sudo firewall-cmd --permanent --add-service=samba`  
`sudo firewall-cmd --reload`  
3. Meerdere domeinen read en write-access geven  
`sudo setsebool -P samba_export_all_rw 1`  
4. `sudo chmod 770 alpha`
5. `sudo chmod 775 bravo`
6. Charlie toevoegen aan de groepen van Alice `sudo usermod -a -G charlie alice`
7. Controleren van de config-file `vi /etc/samba/smb.conf`
8. Voeg in dit bestand de correcte groepen toe aan write list, read list en valid users

```
[alpha]
  comment = alpha
  path = /srv/shares/alpha
  public = no
  write list = @alpha
  read list = @alpha
  valid users = @alpha
  directory mode = 0770

[bravo]
  comment = bravo
  path = /srv/shares/bravo
  public = no
  write list = @bravo
  read list = @bravo, alice
  directory mode = 0770

[charlie]
  comment = charlie
  path = /srv/shares/charlie
  public = no
  read list = @charlie
  directory mode = 0770

[delta]
  comment = delta
  path = /srv/shares/delta
  public = no
  write list = alice
  read list = @delta
  directory mode = 0770

[echo]
  comment = echo
  path = /srv/shares/echo
  public = no
  valid users = @echo

[foxtrot]
  comment = foxtrot
  path = /srv/shares/foxtrot
  public = no
  valid users = @foxtrot
  write list = @foxtrot
  read list = @foxtrot
  directory mode = 0770
```

### Testplan en -rapport

- Run het testscript samba.bats `./runbats.sh`
Alle tests slagen

- Ga op de lokale machine naar `smb://brokensamba/`
Alle shares zijn zichtbaar op de lokale machine

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Het meest voor de hand liggende was dat er dingen gingen moeten aangepast worden in het bestand smb.conf. Dit begon ik dan ook direct te doen
- Ik wist nog redelijk veel over permissies aangezien we dit vroeger geleerd hebben.

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- Door in het begin in het wilde weg wat te beginnen aanpassen had ik op het einde meer moeite om alles juist te krijgen.

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

- Op het lokaal systeem de shares bekijken
- Werken met gebruikersgroepen om permissies toe te kennen

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
