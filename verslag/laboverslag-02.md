## Laboverslag: LAMP

- Naam cursist: Dennis Verfaillie
- Bitbucket repo: https://bitbucket.org/DennisVerfaillie/enterprise-linux-labo

### Procedures

1. Voeg de rollen bertvv.httpd, bertvv.mariadb en bertvv.wordpress toe aan site.yml
2. Voeg onderstaande code toe aan all.yml

 el7_firewall_allow_services:
 
  - http
  - https
  
 httpd_scripting: 'php'
 
 mariadb_databases:
 
  - wordpress
  
 mariadb_root_password: Teilvadci
 
 mariadb_users:
 
   - name: wordpressuser
     password: BulpashCod
     priv: 'wordpress.*:ALL'
     
 wordpress_database: wordpress
 
 wordpress_user: wordpressuser
 
 wordpress_password: BulpashCod
 
3. Voeg onderstaande code toe aan vagrant_hosts.yml

 synced_folders: 
 
    - src: www 
    
      dest: /var/www/html 
      
      options: 
      
        :owner: root 
        
        :group: root 
        
        :mount_options: ['dmode=0755', 'fmode=0644'] 
 
4. run vagrant provision

### Testplan en -rapport

- Surf naar 192.0.2.50/wordpress en de wordpress pagina zou moeten verschijnen

  -> de pagina verschijnt, wordpress werkt net zoals de LAMP stack
  
- Run het testscript lamp.bats

  -> sudo /vagrant/test/runbats.sh
  -> 15 van de 16 punten zijn in orde op dit moment.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Aangezien we vorig jaar al eens een LAMP-stack moesten installeren voor het project wist ik vlug wat er precies allemaal verwacht werd en hoe het moest.
- Danzkij de rollen die we gewoon kunnen gebruiken gaat het werk heel vlot vooruit.
- Ondertussen ben ik het werken met vagrant en ansible al gewoon.

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

- Eerst niet gedacht aan die rollen toevoegen.
- Mijn eigen wachtwoord gebruikt voor wordpress waardoor de pagina niet verscheen
- Werkt blijkbaar niet in microsoft edge.. heb lang daarop blijven proberen

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

- Dat je best eerst eens logisch nadenkt over een probleem voor je in het wilde weg probeert om het op te lossen
- Soms is het best om de machine gewoon eens te verwijderen en opnieuw aan te maken.

#### Waar heb je nog problemen mee?

- Op dit moment heb ik nog geen self-signed certificate gebruikt..

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.

- https://wiki.centos.org/HowTos/Https