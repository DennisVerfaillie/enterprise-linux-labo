## Cheat sheet en checklists

- Naam student: Dennis Verfaillie
- Bitbucket repo: bitbucket.org/DennisVerfaillie/enterprise-linux-labo

Vul dit document zelf aan met commando's die je nuttig vindt, of waarvan je merkt dat je het verschillende keren moeten opzoeken hebt. Idem voor checklists voor troubleshooting, m.a.w. procedures die je kan volgen om bepaalde problemen te identificeren en op te lossen. Voeg waar je wil secties toe om de structuur van het document overzichtelijk te houden. Werk dit bij gedurende heel het semester!

Je vindt hier alvast een voorbeeldje om je op weg te zetten. Zie [https://github.com/bertvv/cheat-sheets](https://github.com/bertvv/cheat-sheets) voor een verder uitgewerkt voorbeeld.

### Commando's

| Taak                   | Commando |
| :---                   | :---     |
| IP-adress(en) opvragen | `ip a`   |
| Systeem stoppen en niet opnieuw opstarten | `shutdown -h now` |
| Systeem opnieuw opstarten | `reboot` |
| Files zoeken | `find / -name fname` |
| Lijst van files in directory | `ls -l` |
| Lijst van files met bestandstype | `ls -F` |
| Toon laatste 10 lijnen van systeemlog | `tail -f /var/log/messages` |
| Verwijder directory name | `rm name` |
| Verwijder directory met alle subdirectories | `rm -rf name` |
| Kopieer bestand naar een directory | `cp filename /home/dirname` |
| Verplaats bestand naar een directory | `mv filename /home/dirname` |
| Voeg een gebruiker toe | `adduser accountname` |
| Geef accountname een nieuw paswoord | `passwd accountname` |
| Geef lijst ip-adressen van alle devices | `ifconfig` |


### Belangrijke configuratiebestanden

| Bestand                | Locatie |
| :---                   | :---     |
| Variabelen voor alle gebruikers | `/etc/profile` |
| Message of the day | `/etc/motd` |
| Parameters voor Apache | `/etc/httpd/conf` |
| Lijst met alle hostnamen en IP-addressen | `/etc/hosts` |
| IP adressen van de DNS servers | `/etc/resolv.conf` |
| Configuratiebestand SAMBA | `/etc/smb.conf` |


### Git workflow

Eenvoudige workflow voor een éénmansproject

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Huidige toestand project                    | `git status`              |
| Bestanden toevoegen/klaarzetten voor commit | `git add FILE...`         |
| Bestanden "committen"                       | `git commit -m 'MESSAGE'` |
| Synchroniseren naar Bitbucket               | `git push`                |
| Wijzigingen van Bitbucket halen             | `git pull`                |

### Vagrant commando's

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Verwijder de vagrant machine | `vagrant destroy` |
| Stop de vagrant machine | `vagrant halt` |
| Toon de help | `vagrant help` |
| Provision de vagrant machine | `vagrant provision` |
| Herstart de vagrant machine | `vagrant reload` |
| Start een gepauzeerde machine | `vagrant resume` |
| Suspend de machine | `vagrant suspend` |
| Start de vagrant omgeving | `vagrant up` |
| Toon de versie van vagrant | `vagrant version` |

### Configureer Git

link Git met je bitbucket account

```
$ git config --global user.name ”BITBUCKET_USERNAME”
$ git config --global user.email ”EMAILADRES@student.hogent.be”
$ git config --global push.default simple
```

zorg voor toegang tot je repositories met een ssh-sleutelpaar

	-	ssh-keygen -t rsa
	-	kies standaard locatie en stel een pass phrase in
	-	open id_rsa.pub bestand en kopieer je ssh-key
	-	ga naar je bitbucket en plak de sleutel op de pagina SSH

Maak een lokale kopie van je eigen repository

`$ git clone --config core.autocrlf=input git@bitbucket.org:BITBUCKET_USERNAME/REPO.git`

### Rollen toekennen aan hosts

```
---
- hosts: pu004
  sudo: true
  roles:
	- bertvv.el7
```

### Rollen installeren

`scripts/role-deps.sh`

### Configureren hosts

```
---
var1: val1
var2:
  - val21
  - val22
var3:
  - key311: val311
	key312: val312
  - key321: val321
    key322: val322
var4: val4
```


### Ansible rollen aanmaken

Voorbeeld om packages te installeren

```
- name: Install packages
	yum:
		pkg: ”{{ item }}”
		state: present
	with_items: vsftpd_packages
	tags: vsftpd
```

Voorbeeld configuratiebestand installeren en valideren

```
- name: Install Vsftpd config file
	template:
		src: vsftpd.conf.j2
		dest: /etc/vsftpd/vsftpd.conf
	validate: ’vsftpd -olisten=NO %s’
	tags: vsftpd
```


### Checklist netwerkconfiguratie

1. Is het IP-adres correct? `ip a`
2. Is de router/default gateway correct ingesteld? `ip r -n`
3. Is er een DNS-server? `cat /etc/resolv.conf`

### Checklist Troubleshooting

1. Is er een IP-aders ingesteld? `ip a`

	- e.g. vbox NAT: 10.0.2.15  
	- e.g. vbox host-only: 192.168.56.101  
	- /etc/sysconfig/network-scripts/ifcfg-IFACE  
	
	Indien resultaat: NO-CARRIER: misschien de kabel insteken?  

	Nog niet opgelost -->

2. Machine zelf 

	-	Ip-adres en netwerkmasker  
	-	Default-gateway  

3. Probeer dit commando: `Ip r`

	-	 Vbox NAT: 10.0.2.2  
	-	 Thuisnetwerk: 192.168.0.1  

4. Dns-server

	-	 Controleer dit bestand: `Cat /etc/resolv.conf`  
	-	 Nat: 10.0.2.3  
	-	 Thuis: zelfde als gateway  
	-	Default gateway: `ping`  
	-	Dns request: evt `ping`, `dig`, `host`, `nslookup`  
	-	Ping andere host binnen subnet  
	-	Traceroute naar buiten    
	-	`Traceroute`, `tracert`

5. Zijn de juiste poorten open/draait de service

	-	`Sudo systemctl status httpd.service`  
	-	`Sudo ss –tulpn`: 80, 443 zeker open  
	-	`Sudo ps –ef`  

6. Wordt de poort gefilterd door de firewall?

	-	`Sudo firewall-cmd –list-all`  
	-	`Sudo iptables –L –n –v`  

7. Bekijk de logfiles

	-	`Sudo journalctl –l –f –u httpd.service`  

8. Valideer configuratiebestanden

	-	Dns: `named-checkconf`, `named-checkzone`
	
### Checklist Samba

1. Zorg dat smb en nmb runnen

	- 	`sudo systemctl start smb/nmb`

2. Zorg ervoor dat Samba toegevoegd is aan je firewall

	- 	`sudo firewall-cmd --permanent --add-service=samba`
		`sudo firewall-cmd --reload`

3. Meerdere domeinen Read en Write access geven

	- 	`sudo getsebool -a | grep \samba` --> zorg ervoor dat exprt_all_rw op on staat
		`sudo setsebool -P samba_export_all_rw 1`
		
4. Zorg dat in de map /srv/shares alle bestanden de juiste permissies hebben

	- 	`sudo chmod 775 /srv/shares/(mapnaam)`
	
5. Voeg een gebruiker toe aan een groep zodat hij toegang kan krijgen tot een map

	- 	`sudo usermod -a -G charlie alice`
	
6. Controleer of de config file correct is aangevuld

	- 	`sudo vi /etc/samba/smb.conf`


### Vaak voorkomende fouten

• Hostnamen die volledig uitgeschreven zijn (fully qualified domain name/FQDN) moeten in een zonebestand altijd
afgesloten worden met een punt, bv. “pu001.linuxlab.net.”. Dit is één van de meest voorkomende fouten bij het
configureren van BIND. Namen die niet op een punt eindigen, worden aangevuld met de waarde van $ORIGIN die aan het
begin van een zonebestand gegeven wordt (d.i. de domeinnaam, in ons geval “linuxlab.net.”). Bv. pu002 wordt dan
“pu002.linuxlab.net.”. Als je een hostnaam volledig uitschrijft en je vergeet het punt, dan zal de domeinnaam dus
verkeerd geïnterpreteerd worden (“pu002.linuxlab.net” wordt immers “pu002.linuxlab.net.linuxlab.net.”).  

• IP-adressen worden op een eigenaardige manier genoteerd. Ten eerste wordt het host-deel van het netwerkadres
niet genoteerd, de getallen in de “dotted quad”-notatie worden omgekeerd en je moet er “in-addr.arpa.” achter
schrijven. Met andere woorden, 192.0.2.0/24 wordt als “2.0.192.in-addr-arpa.” geschreven.

